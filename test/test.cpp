#include <iostream>
#include <gtest/gtest.h>

#include "matmul.h"

int main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

TEST(matmul, matmul) {

    // int M = 32* 1;
    // int N = 32 * 5;
    // int K = 32 * 6;

    int M = 1000;
    int N = 2000;
    int K = 3000;

    srand(0);

    float* A = (float*)malloc(sizeof(float) * M * K);
    float* B = (float*)malloc(sizeof(float) * K * N);
    float* C = (float*)malloc(sizeof(float) * M * N);
    float* C_ref = (float*)malloc(sizeof(float) * M * N);

    for (int i = 0; i < M * K; i++) {
        A[i] = rand() % 256;
    }

    for (int i = 0; i < K * N; i++) {
        B[i] = rand() % 256;
    }

    int type = 0;
    matmul<float>(A, B, C, M, K, N, type);

    // compute matrix mulplication on CPU for reference, this may take a while.
    for (int i = 0; i < M; i ++) {
        for (int j = 0; j < N; j++) {
            float sum = 0;
            for (int k = 0; k < K; k++) {
                sum += A[i * K + k] * B[k * N + j];
            }
            C_ref[i * N + j] = sum;
        }
    }

    for (int i = 0; i < M * N; i++) {
        // std::cout << "i: " << i << std::endl;
        ASSERT_FLOAT_EQ(C[i], C_ref[i]);
    }

    free(A);
    free(B);
    free(C);
}