
CUDA example for matrix multiplication

Usage:

```bash
mkdir build
cd build
cmake .. -DGTEST_LIBRARY=/path/to/gtest.lib -DGTEST_MAIN_LIBRARY=/path/to/gtest_main.lib -DGTEST_INCLUDE_DIR=/path/to/gtest/include
cmake --build .
```

the `test_matmul.exe` file will be generated in `./test/Debug` directory.