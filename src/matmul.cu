#include <stdio.h>
#include <iostream>
#include <cuda_runtime.h>

#include "matmul.h"

// baseline
template <typename scalar_t>
__global__ void matmul_kernel_0(scalar_t* A, scalar_t* B, scalar_t* C, int M, int K, int N) {

    int row = blockIdx.y * BLOCK_SIZE + threadIdx.y;
    int col = blockIdx.x * BLOCK_SIZE + threadIdx.x;

    if(row >= M || col >= N) return;

    scalar_t sum = 0;
    for(int k = 0; k < K; k++) {
        sum += A[row * K + k] * B[k * N + col];
    }
    C[row * N + col] = sum;

}

// shared memory
template <typename scalar_t>
__global__ void matmul_kernel_1(scalar_t* A, scalar_t* B, scalar_t* C, int M, int K, int N) {

    int row = blockIdx.y * BLOCK_SIZE + threadIdx.y;
    int col = blockIdx.x * BLOCK_SIZE + threadIdx.x;


    __shared__ scalar_t As[BLOCK_SIZE * BLOCK_SIZE];
    __shared__ scalar_t Bs[BLOCK_SIZE * BLOCK_SIZE];

    int numBlocks = (K + BLOCK_SIZE - 1) / BLOCK_SIZE;

    scalar_t sum = 0;
    for (int i = 0; i < numBlocks; i++) {

        int x_A = i * BLOCK_SIZE + threadIdx.x;
        int y_A = blockIdx.y * BLOCK_SIZE + threadIdx.y;
        
        if(x_A < K && y_A < M) {
            As[threadIdx.y * BLOCK_SIZE + threadIdx.x] = A[y_A * K + x_A];
        }else{
            As[threadIdx.y * BLOCK_SIZE + threadIdx.x] = 0;
        }

        int x_B = blockIdx.x * BLOCK_SIZE + threadIdx.x;
        int y_B = i * BLOCK_SIZE + threadIdx.y;
        if(x_B < N && y_B < K) {
            Bs[threadIdx.y * BLOCK_SIZE + threadIdx.x] = B[y_B * N + x_B];
        }else{
            Bs[threadIdx.y * BLOCK_SIZE + threadIdx.x] = 0;
        }
        __syncthreads();

        for(int k = 0; k < BLOCK_SIZE; k++) {
            sum += As[threadIdx.y * BLOCK_SIZE + k] * Bs[k * BLOCK_SIZE + threadIdx.x];
        }

        __syncthreads();

    }

    if(row >= M || col >= N) return;

    C[row * N + col] = sum;

}

// template specialization
template void matmul<float>(float* A, float* B, float* C, int m, int k, int n, int type);
template void matmul<double>(double* A, double* B, double* C, int m, int k, int n, int type);
template void matmul<int>(int* A, int* B, int* C, int m, int k, int n, int type);


template <typename scalar_t>
void matmul(scalar_t* A, scalar_t* B, scalar_t* C, int M, int K, int N, int type){

    int grid_width = (M + BLOCK_SIZE - 1) / BLOCK_SIZE;
    int grid_height = (N + BLOCK_SIZE - 1) / BLOCK_SIZE;

    std::cout<<"grid_height: "<<grid_height<<std::endl;
    std::cout<<"grid_width: "<<grid_width<<std::endl;

    dim3 block(BLOCK_SIZE, BLOCK_SIZE);
    dim3 grid(grid_height, grid_width);

    scalar_t* d_A;
    scalar_t* d_B;
    scalar_t* d_C;

    size_t size_A = M * K * sizeof(scalar_t);
    size_t size_B = K * N * sizeof(scalar_t);
    size_t size_C = M * N * sizeof(scalar_t);

    cudaMalloc((void**)&d_A, size_A);
    cudaMalloc((void**)&d_B, size_B);
    cudaMalloc((void**)&d_C, size_C);

    cudaMemcpy(d_A, A, size_A, cudaMemcpyHostToDevice);
    cudaMemcpy(d_B, B, size_B, cudaMemcpyHostToDevice);

    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    cudaEventRecord(start);
    switch (type)
    {
    case 0:
        matmul_kernel_0<<<grid, block>>>(d_A, d_B, d_C, M, K, N);
        break;
    case 1:
        matmul_kernel_1<scalar_t><<<grid, block>>>(d_A, d_B, d_C, M, K, N);
        break;
    
    default:
        break;
    }
    cudaDeviceSynchronize();
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);

    float milliseconds = 0;
    cudaEventElapsedTime(&milliseconds, start, stop);
    std::cout<<"time: "<<milliseconds << "ms" << std::endl;

    cudaMemcpy(C, d_C, size_C, cudaMemcpyDeviceToHost);

    cudaFree(d_A);
    cudaFree(d_B);
    cudaFree(d_C);    

}