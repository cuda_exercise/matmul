
#define BLOCK_SIZE 32


template <typename scalar_t>
void matmul(scalar_t* A, scalar_t* B, scalar_t* C, int M, int K, int N, int type);